package com.gbh.demo.persistence.repository;

import com.gbh.demo.persistence.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {
}
